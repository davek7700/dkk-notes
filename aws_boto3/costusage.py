import boto3
import pprint

session = boto3.Session(profile_name='dicelab')
client = session.client('ce')

response = client.get_cost_and_usage(
    TimePeriod={
        'Start': '2022-01-01',
        'End': '2022-11-30'
    },
    Metrics=['BlendedCost'],
    Granularity='MONTHLY',
    GroupBy=[
        {
            'Type': 'DIMENSION',
            'Key': 'SERVICE'
        }
    ]
)

for timeperiod in response['ResultsByTime']:
    #print(timeperiod)
    #print(timeperiod['TimePeriod']['Start'])
    #print(timeperiod['Groups'])
    for service in timeperiod['Groups']:
        print(timeperiod['TimePeriod']['Start'],end='')
        print(",",end='')
        print(service['Keys'],end='')
        print(",",end=''),
        print(service['Metrics']['BlendedCost']['Amount'])
        #print(service['Metrics]['BlendedCost]['Amount'])

#print(len(response['ResultsByTime']))
#print(response)
#pprint.pprint(response['GroupDefinitions'])