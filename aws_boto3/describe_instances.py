import boto3

client = boto3.client('ec2')

resp = client.describe_instances()

def getVolumeInformation(volId: str) -> str:
    resp2 = client.describe_volumes(Filters=[{
        'Name': 'volume-id',
        'Values': [volId]
    }])
    #print(resp2['Volumes'][0]['VolumeId'])
    #print(resp2['Volumes'][0]['Size'])
    #print(resp2['Volumes'][0]['VolumeType'])

    #print(resp2['Volumes'][1]['Attachments'])
    #print(len(resp2['Volumes'][0]['Attachments']))
    #print("\n\n")
    return resp2['Volumes'][0]['VolumeId'] + "," + str(resp2['Volumes'][0]['Size']) + "," + resp2['Volumes'][0]['VolumeType']

def devices2string(blockdevice: list) -> str:
    volumeList = []
    for device in blockdevice:
        #print(device['Ebs']['VolumeId'])
        #volumeList.append(device['Ebs']['VolumeId'])

        volumeList.append(getVolumeInformation(device['Ebs']['VolumeId']))


    return ",".join(volumeList)

def getNameTag(tags: list) -> str:

    for tag in tags:
        if 'Name' in tag['Key']:
            return tag['Value']

    return "NAME TAG NOT FOUND"

for reservation in resp['Reservations']:
    for instance in reservation['Instances']:
        ## print tests for function
        # #print("instanceId is: ".format(instance['InstanceId']))
        #print(instance['InstanceId'])
        # print(type(instance['BlockDeviceMappings']))
        # print(instance['BlockDeviceMappings'])
        # print(instance['InstanceId'])
        #print("instanceId is: {} and it has {} volumes".format(instance['InstanceId'],len(instance['BlockDeviceMappings'])))
        # print("instanceId is: {} has the following tags: {}".format(instance['InstanceId'], instance['Tags']))
        # print("instanceId is: {} has the nAME tags: {}".format(instance['InstanceId'], instanceName))
        # print("instanceId is: {} is: {}".format(instance['InstanceId'], instance['State']['Name']))
        # print("instanceId is: {} platform is: {}".format(instance['InstanceId'], instance['PlatformDetails']))
        #print("instanceId is: {} has the following volumes: {}".format(instance['InstanceId'],vList))

        # get volume list in format:  volumeID, size, VolumeType
        vList = devices2string(instance['BlockDeviceMappings'])

        # get instance from the 'Name' Tag
        try:
            instanceName = getNameTag(instance['Tags'])
        except:
            instanceName = "NoNameTag"

        # print out wanted information
        print("{},{},{},{},{},{}".format(instanceName,instance['InstanceId'],instance['InstanceType'],instance['State']['Name'],instance['PlatformDetails'],vList))