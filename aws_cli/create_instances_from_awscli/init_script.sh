#!/bin/bash
hostnamectl set-hostname cephosd03.autumnal.local
hostname > /etc/salt/minion_id
systemctl enable salt-minion --now
