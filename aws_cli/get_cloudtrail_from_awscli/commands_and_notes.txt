Problem to solve:
-----------------

AWS cloud trail was configured to alert the Security team if any of the Events in "eventnames.txt" were triggered in a five minute period.  The alarm does not include the metadata (ie - what happened), but rather just the fact that they were triggered.  The one-line below assumes all of your `aws configured` steps have been taken and run and `aws_access_key_id`,  `aws_secret_access_key` and `region` have been set.  The one liner will then search for the full data log for the eventname between the `--start-time` and `--end-time`.


Setup Security team with read-only aws cli access and transfered bash scripts, tools and knowledge of how to retrieve metadata necessary for security adjudication.


basic bash one-liner:
---------------------
for event in $(cat eventnames.txt); do echo $event ; echo "---" ; aws cloudtrail lookup-events --lookup-attributes AttributeKey=EventName,AttributeValue=$event --start-time "06/01/2022 00:00:00"  --end-time "06/01/2022 23:59:59" ; done

Need the "eventnames.txt"

